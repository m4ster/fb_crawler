mongodb = require('mongodb')
settings = require('../settings')
Db = mongodb.Db
Connection = mongodb.Connection
Server = mongodb.Server
require('events').EventEmitter.prototype._maxListeners = 4096


mongodb = new Db(settings.db, new Server(settings.host, settings.port), {safe: true})

module.exports = mongodb