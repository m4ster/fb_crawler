mongodb = require('./db')
debug = require('debug')('crawler:article')


Article = (article) ->
  @id = article.id
  @title = article.title
  @author = article.author
  @url = article.url
  @desc = article.desc
  @time = article.time
  @rank = article.rank
  @tags = article.tags
  @content = article.content

module.exports = Article

###
# Article save
# @param {Function} callback
###
Article.prototype.save = (callback) ->
  $article = {
    id: @id
    title: @title
    author: @author
    url: @url
    desc: @desc
    time: @time
    rank: @rank
    tags: @tags
    content: @content
  }
  mongodb.open((err, db) ->
    if err
      return callback(err)
    db.collection('article', (err, collection) ->
      if err
        mongodb.close()
        return callback(err)

      # 查询数据库中是否有记录
      collection.find({id: $article.id}).toArray((err, articles) ->
        if err
          mongodb.close()
          return callback(err)
        # 无
        if not articles.length
          collection.insert(
            $article
            {safe: true}
            (err) ->
              mongodb.close()
              if err
                return callback(err)
              debug '添加文章：%s', $article.url
              return callback(null)
          )
        # 有
        else
          debug '更新文章：%s', $article.url

          collection.update({
            'article.id': $article.id
            }, {
              $set: $article
            },(err) ->
              mongodb.close()
              if err
                return callback(err)
              callback(null)
          )
      )
    )
  )

###
# Article get all
# @param {Function} callback
###
Article.getAll = (callback) ->
  debug '读取全部文章内容'
  mongodb.open((err, db) ->
    if err
      return callback(err)
    db.collection('article', (err, collection) ->
      if err
        mongodb.close()
        return callback(err)
      collection.find({}).sort({
        time: -1
      }).toArray((err, articleList) ->
        mongodb.close()
        if err
          return callback(err)
        return callback(null, articleList)
      )
    )
  )
