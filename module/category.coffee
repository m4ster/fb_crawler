mongodb = require('./db')
debug = require('debug')('crawler:category')


category = ($category) ->
  @name = $category.name
  @url = $category.url

module.exports = category

###
# category save
# @param {Function} callback
###
category.prototype.save = (callback) ->
  $category = {
    name: @name
    url: @url
  }
  mongodb.open((err, db) ->
    if err
      return callback(err)
    db.collection('category', (err, collection) ->
      if err
        mongodb.close()
        return callback(err)

      # 查询数据库中是否有记录
      collection.find($category).toArray((err, categories) ->
        # 无
        if not categories.length
          # 保存此分类
          debug '添加分类：%s', $category.name
          collection.insert(
            $category
            {safe: true}
            (err) ->
              mongodb.close()
              if err
                return callback(err)
              return callback(null)
          )
        # 有
        else
          debug '更新分类：%s', $category.name

          collection.update({'category.url': $category.url}, {
            $set: $category
          }, (err) ->
            mongodb.close()
            if err
              return callback(err)
            callback(null)
          )
      )
    )
  )

###
# category get all
# @param {Function} callback
###
category.prototype.getAll = (callback) ->
  debug '读取全部文章分类'
  mongodb.open((err, db) ->
    if err
      return callback(err)
    db.collection('category', (err, collection) ->
      if(err)
        mongodb.close()
        return callback(err)
      collection.find({}).toArray((err, category) ->
        mongodb.close()
        if err
          return callback(err)
        callback(null, category)
      )
    )
  )
