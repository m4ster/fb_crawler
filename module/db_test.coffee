mongodb = require('./db')

# category name
$category = {
  name: process.argv[2]
  url: 'http://test'
}

mongodb.open((err, db) ->
  if err
    return console.error(err)
  # 连接数据库
  db.collection('category', (err, collection) ->
    # 查询记录
    collection.find($category).toArray((err, category) ->
      if err
        return console.error(err)
      # 无记录
      if not category.length
        console.log('Not exists. Add: ', $category)
        collection.insert(
          $category
          {safe: true}
          (err) ->
            mongodb.close()
            if err
              return console.error err
        )
      else
        mongodb.close()
    )
  )
)
