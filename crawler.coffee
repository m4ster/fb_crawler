#!/usr/bin/env coffee
originRequest = require('request')
cheerio = require('cheerio')
debug = require('debug')('crawler:main')
async = require('async')

Article = require('./module/article')
Category = require('./module/category')
settings = require('./settings')


global.articles = []
#process.setMaxListeners(0)

headers = {
  'User-Agent': 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)'
  'Referer': 'http://www.freebuf.com/'
}

request = (url, callback) ->
  options = {
    url: url
    timeout: 60000,
    headers: headers
  }
  return originRequest(options, callback)


###
# 读取文章分类
# @param {String} url
# @param {Function} callback
###
readArticleCategory = (url, callback) ->
  debug '读取文章分类'
  request(url, (err, response, body) ->
    if err
      return callback(err)
    if response.statusCode isnt 200
      return callback("Status Code: #{response.statusCode}")

    # 根据网页内容创建 DOM 操作对象
    $ = cheerio.load(body)

    # 全部分类
    categories = []

    # 读取文章分类
    $('.dropdown-menu.article-nav li a').each(() ->
      $me = $(this)
      item = {
        name: $me.text().trim()
        url: $me.attr('href')
      }

      # 从 URL 中提取分类的 ID
      s = item.url.match(/www.freebuf.com\/(.*)/)
      if Array.isArray(s)
        item.id = s[1].replace(/\//g, '.')
        categories.push(item)
      else
        return callback('Can\'t find the category id. ' + item.url)
    )

    # 返回结果
    return callback(null, categories)
  )

###
# 读取文章列表
# @param {String} url
# @param {Function} callback
###
readArticleList = (url, callback) ->
  debug '读取文章列表：%s', url

  request(url, (err, response, body) ->
    if err
      return callback(err)

    # 根据网页内容创建 DOM 操作对象
    if response.statusCode isnt 200
      return callback("Status Code: #{response.statusCode}")

    $ = cheerio.load(body)

    # 读取文章列表
    articleList = []
    $('.news_inner.news-list').each(() ->
      $me = $(this)
      item = {
        img: $me.find('img').attr('src')
        title: $me.find('a').text().trim()
        url: $me.find('a').attr('href')
        desc: $me.find('.text').text().trim()
      }

      # 从 URL 中读取文章 ID
      s = item.url.match(/\/(\d+)\.html/)
      if Array.isArray(s)
        item.id = s[1]
        articleList.push(item)
      else
        return callback('Can\'t find the article id. ' + item.url)
    )

    # 检测是否有下一页
    callbackUrl = $('#pagination a').attr('href')

    # 没有下一页 / 最后一页
    if not callbackUrl
      # 返回结果
      return callback(null, articleList)

    # 读取下一页
    readArticleList(callbackUrl, (err, articleList2) ->
      if err
        return callback(err)

      # 返回结果
      return callback(null, articleList.concat(articleList2))
    )
  )


###
# 读取文章内容
# @param {String} url
# @param {Function} callback
###
readArticleDetail = (url, callback) ->
  debug '读取文章内容：%s', url

  request(url, (err, response, body) ->
    if err
      return callback(err)

    if response.statusCode isnt 200
      return callback("Status Code: #{response.statusCode}")

    # 根据网页内容创建 DOM 操作对象
    $ = cheerio.load(body, {decodeEntities: false})

    # 文章详情
    detail = {
      id: Number($('#comment_post_ID').val())
      title: $('.title > h2').text().trim()
      author: $('.property > .name').text().trim()
      time: new Date($('.property > .time').text().trim()).getTime()
      rank: Number($('.look > strong').text().trim())
      content: $('#contenttxt').html().trim()
    }

    # 读取文章标签
    tags = []
    $('.tags a').each(() ->
      $me = $(this)
      tags.push($me.text().trim())
    )
    detail.tags = tags

    # 返回结果
    return callback(null, detail)
  )


async.series(
  [
    # 读取文章分类列表
    (done) ->
      readArticleCategory(settings.website.url, (err, $categories) ->
        global.categories = $categories
        async.eachSeries($categories, ($category, callback) ->
          # 保存文章分类列表
          newCategory = new Category($category)
          newCategory.save callback
        , done)
      )

    # 读取分类下的文章列表
    (done) ->
      async.each(categories, ($category, callback) ->
        readArticleList($category.url, (err, $articles) ->
          global.articles = articles.concat($articles)
          return callback(err)
        )
      , done)

    # 保存分类下的文章列表
    (done) ->
      async.each(articles, ($article, callback) ->
        readArticleDetail($article.url, (err, $detail) ->
          if err
            return callback(err)
          # 文章详情
          $article.id = $detail.id
          $article.title = $detail.title
          $article.author = $detail.author
          $article.time = $detail.time
          $article.rank = $detail.rank
          $article.tags = $detail.tags
          $article.content = $detail.content
          # 录入文章
          newArticle = new Article($article)
          newArticle.save callback
        )
      , done)

  ],(err) ->
    if(err)
      return console.error(err)

    debug 'OK. Time at: %s', new Date()
    process.exit(0)
)


#async.series(
#  [
#    # 读取文章分类列表
#    (done) ->
#      readArticleCategory(settings.website.url, (err, $categories) ->
#        global.categories = $categories
#        async.eachSeries($categories, ($category, callback) ->
#          # 保存文章分类列表
#          newCategory = new Category($category)
#          newCategory.save callback
#        , done)
#      )
#
#    # 读取分类下的文章列表
#    (done) ->
#      async.eachSeries(categories, ($category, callback) ->
#        readArticleList($category.url, (err, $articles) ->
#          global.articles = articles.concat($articles)
#          callback(err)
#        )
#      , done)
#
#    # 保存分类下的文章列表
#    (done) ->
#      async.eachSeries(articles, ($article, callback) ->
#        readArticleDetail($article.url, (err, $detail) ->
#          if err
#            return callback(err)
#          # 文章详情
#          $article.id = $detail.id
#          $article.title = $detail.title
#          $article.author = $detail.author
#          $article.time = $detail.time
#          $article.rank = $detail.rank
#          $article.tags = $detail.tags
#          $article.content = $detail.content
#          # 录入文章
#          newArticle = new Article($article)
#          newArticle.save callback
#        )
#      , done)
#
#  ],(err) ->
#    if(err)
#      return console.error(err)
#
#    console.log('OK.')
#    process.exit(0)
#)
